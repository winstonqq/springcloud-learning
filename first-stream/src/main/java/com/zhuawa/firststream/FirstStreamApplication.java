package com.zhuawa.firststream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

@SpringBootApplication
@EnableBinding(Sink.class)
public class FirstStreamApplication {

    private static final Logger logger = LoggerFactory.getLogger(FirstStreamApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(FirstStreamApplication.class, args);
    }

    @StreamListener(Sink.INPUT)
    public void receive(Object payload) {
        logger.info("Received: " + payload);
    }
}
